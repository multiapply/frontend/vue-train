import { shallowMount } from '@vue/test-utils'
import ProgressBar from '../ProgressBar.vue'

describe('ProgressBar.vue', () => {
	it('is hidden on initial render', () => {
		const wrapper = shallowMount(ProgressBar)
		expect(wrapper.classes()).toContain('hidden');
	})

  it('initializes with 0% width', () => {
    const wrapper = shallowMount(ProgressBar)
    expect(wrapper.element.style.width).toBe('0%')
  })
  
})