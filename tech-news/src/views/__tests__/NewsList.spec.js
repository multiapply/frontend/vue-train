import { shallowMount, mount } from '@vue/test-utils'
import NewsList from '../NewsList.vue'
import NewsPost from '../../components/NewsPost.vue'

describe('NewsList.vue', () => {
	it('renders a NewsPost with data for each post in window.posts', () => {
    window.posts = [{},{},{}]
    
    const wrapper = shallowMount(NewsList)
    const posts = wrapper.findAllComponents(NewsPost)

    expect(posts).toHaveLength(window.posts.length)
    posts.forEach((wrapper, i) => {
      expect(wrapper.vm.post).toEqual(window.posts[i])
    })
	})
})

