import { shallowMount } from '@vue/test-utils'
import NewsPost from '../NewsPost.vue'

describe('NewsPost.vue', () => {
	it('renders post.score', () => {
		const post = {
			score: "10"
		}
		
		const wrapper = shallowMount(NewsPost, {
			propsData: { post }
		})
		expect(wrapper.text()).toContain(post.score)
	})

	it('renders a link to the post.url with post.title as text', () => { 
		const post = {
			url: 'http://some-url.com',
			title: 'some title'
		}

		const wrapper = shallowMount(NewsPost, {
			propsData: { post }
		})
		
		const link = wrapper.find('a')
		expect(link.text()).toBe(post.title)
		expect(link.attributes().href).toBe(post.url)

	})
})
